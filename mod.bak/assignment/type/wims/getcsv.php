<?php  // $Id: getcsv.php,v 1.1.1.1 2009-04-04 14:19:23 georgesk Exp $
/**
* @author Georges Khaznadar
* @license http://www.gnu.org/copyleft/gpl.html GNU Public License
* @package wims
*
*
*/

$f=fopen($_GET['url'],"r");
$lines=explode("\n",fread($f,32*1024));
header("Content-Encoding: iso-8859-1");
header("Content-Type: text/plain");
foreach($lines as $l){
  print ("$l\n");
}


?>