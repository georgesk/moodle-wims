<?php // $Id: wimsclass.php,v 1.1.1.1 2009-04-04 14:19:23 georgesk Exp $

require_once($CFG->libdir.'/datalib.php'); // for get_record(...)

/**
 * The class wc_data encapsulates the format of the request necessary
 * to create a Wims class.
 * @author: Georges Khaznadar
 *
 * initialisation:  wc_data($rclass,$service,$servicepass)
 * parameters:
 * * $rclass is the name of the class in Moodle
 * * There must be a file wimshome/log/classes/.connections/$service
 * * with a line defining: 'ident_password=$servicepass'
 *
 * an accessor: toString($html=1)
 * this function outputs a string with the object's internals.
 * parameters:
 * $html, when true, fits the output to be displaied in a HTML page.
 *
 * two functions: data1() and data2()
 * these functions prepare the data for the Wims class according to the
 * documentation file http://wims.unice.fr/wims/?module=adm/raw&job=help
 **********************************************************************/

class wc_data{
  var $description;
  var $institution;
  var $firstname;
  var $lastname;
  var $supervisor;
  var $email;
  var $password;
  var $lang;
  var $expiration;
  var $limit;
  var $secure;
  var $qclass;
  var $service;
  var $servicepass;
  function wc_data($assignment,$service,$servicepass){
	global $DB;    
$this->service=$service;
    $this->servicepass=$servicepass;
    $this->description=rclass($assignment);
   // $c1=get_record("course","id",1);
    $c1=$DB->get_record("course",array("id"=>1)); //Recuperation du nom du site moodle

    $this->institution=protection($c1->shortname);
    $context = get_context_instance(CONTEXT_COURSE, $assignment->course);
    $teacherID=3; // 3 is the id for teachers.
    $users = get_role_users($teacherID, $context, true, '', 'u.lastname ASC', true);
    $firstuser=array_shift($users);
    $tid=$firstuser->id;
   // $t=get_record("user","id",$tid);
    $t=$DB->get_record("user",array("id"=>$tid));
    $this->firstname=$t->firstname;
    $this->lastname=$t->lastname;
    $this->supervisor=$t->username;
    $this->email=$t->email;
    $this->password=random_string(8);
    $this->lang=ereg_replace("_.*","",$t->lang);
    $now=time();
    $this->expiration=(date('Y',$now)+1).date('md',$now);
    $this->limit="499";
    $this->secure="all";
    $this->qclass="";
  }
  function toString($html=1){
    if ($html) $spacer="<br />"; else $spacer=" ";
    $result="";
    $result.="description = ".$this->description.$spacer;
    $result.="institution = ".$this->institution.$spacer;
    $result.="firstname = ".$this->firstname.$spacer;
    $result.="lastname = ".$this->lastname.$spacer;
    $result.="supervisor = ".$this->supervisor.$spacer;
    $result.="password = ".$this->password.$spacer;
    $result.="lang = ".$this->lang.$spacer;
    $result.="expiration = ".$this->expiration.$spacer;
    $result.="limit = ".$this->limit.$spacer;
    $result.="secure = ".$this->secure.$spacer;
    return $result;
  }
  function data1(){
    $d1=array('description', 'institution', 'supervisor', 'email', 'password',
	      'lang', 'expiration', 'limit', 'level', 'secure',
	      'bgcolor', 'refcolor', 'css');
    $data1="";
    foreach ($d1 as $d){

     		if (isset($this->$d) && strlen($this->$d)>0){
			$data1.=$d."=".$this->$d."\n";
   		}
	
    }
    return urlencode($data1);
  }
  function data2(){
    $d2=array('lastname', 'firstname', 'useremail', 'comment', 'regnum');
    $data2="";
    foreach ($d2 as $d){
      if (isset($this->$d) && strlen($this->$d)>0){
	$data2.=$d."=".$this->$d."\n";
      }
    }
    $data2.="password=".random_string(8)."\n";// this password will not be used
    return urlencode($data2);
  }
}

/**
 * makes a name for the "Wims class" on Moodle's side
 */

function rclass($assignment){
global $DB; 
 //$c=get_record("course","id",$assignment->course); v1.9 moodle
  $c=$DB->get_record("course",array("id"=>($assignment->course)));
	
  return rclassFromCourseAndModule($c,$assignment);
}

function rclassFromCourseAndModule($c,$m){
  return wimsIconv($c->shortname)."_".wimsIconv($m->name);
}

/**
 * protects a worksheet's contents before calling the job addsheet
 */

function protectSheetContents($contents){
  $contents=str_replace("\n",";",$contents);
  $contents=str_replace("=","@",$contents);
  return $contents;
}

/**
 * the class workSheet encapsulates pentuples (index,title,status,rclass,qclass)
 * AFAIK, the status are the following with WIMS :
 * 0 = in preparation
 * 1 = active worksheet
 * 2 = unactive worksheet
 * 3 = hidden worksheet
 */
class workSheet{
  var $index;
  var $title;
  var $status;
  var $rclass;
  var $qclass;
  var $rclassEnc;

  /**
   * the constructor
   */
  function workSheet($index,$title,$status,$rclass,$qclass){
    $this->index=$index;
    $this->title=$title;
    $this->status=$status;
    $this->rclass=$rclass;
    $this->qclass=$qclass;
    $this->rclassEnc=urlencode($rclass);
  }

  /**
   * @return an url to get the worksheet's file
   * @param $jr : a job_raw object already initialized
   */
  function getUrl($jr){
    $url=$jr->url("getclassfile",
		  "qclass=$this->qclass&rclass=$this->rclassEnc".
		  "&option=sheets/.sheet".$this->index);
    return $url;
  }

  /**
   * @return an url to add the worsheet's contents to
   * a wims assignment's miniclass.
   * @param $assign_wims the wims assignment
   * @param $forbidMD5sums an array of already present md5 sums of contents
   */
  function addsheetToUrl($assign_wims, $forbidMD5sums=array()){
    global $CFG;
	global $DB;
    $contents=$this->contents($assign_wims->jr);
    $md5=md5($contents);
    if (array_intersect($forbidMD5sums,array($md5))) return "";
    $contents=protectSheetContents($contents);
    $qclass=$assign_wims->assignment->var2;
    $course= $DB->get_record("course", array("id"=>$assign_wims->cm->course));
    $rclass=rclassFromCourseAndModule($course, $assign_wims->cm);
    $rclassEnc=urlencode($rclass);
    $parms="qclass=$qclass&rclass=$rclassEnc&data1=" . 
      urlencode("contents=$contents\nsheetmode=3\ntitle=$this->title\n");
    $url=$assign_wims->jr->url("addsheet",$parms);
    return $url;
  }

  /**
   * @return the contents of the worksheet's file
   * @param $jr : a job_raw object already initialized
   */
  function contents($jr){
    global $CFG;
    $url=$this->getUrl($jr);
    $url=urlencode($url);
    $url=$CFG->wwwroot."/mod/assignment/type/wims/getsheetsource.php?url=$url";
    $f=fopen($url,"r");
    if($f){
      $contents=fread($f,20480);
      fclose($f);
    } else {
      $contents="";
    }
    return $contents;
  }
}


/**
 * the class wimsExam encapsulates quadruples (index,title,rclass,qclass)
 * AFAIK, the status are the following with WIMS :
 * 0 = in preparation
 * 1 = active exam
 * 2 = unactive exam
 * 3 = hidden exam
 */
class wimsExam{
  var $index;
  var $i;
  var $title;
  var $rclass;
  var $qclass;
  var $rclassEnc;

  /**
   * the constructor
   */
  function wimsExam($index,$title,$rclass,$qclass){
    $this->index=$index;
    $this->i=substr($index,4,255); // the number after "exam"
    $this->title=$title;
    $this->rclass=$rclass;
    $this->qclass=$qclass;
    $this->rclassEnc=urlencode($rclass);
  }

  function wimsButton($user){
    $result="here should be button to access the exam ".$this->title;
    print "debug user:<pre>";print_r($user->lastname);; print "</pre>";
    return $result;
  }
}

/**
 * converts utf8 strings for WIMS
 * @param s the input string
 * @return a string recoded for Wims
 */

  function wimsIconv($s,$mode="forth"){
      $r=$s;
      $wims_is_not_utf8=TRUE;
      if($wims_is_not_utf8 && function_exists('iconv')){
	if ($mode=="forth") $r=iconv('UTF-8','ISO8859-1',$s);
	else $r=iconv('ISO8859-1','UTF-8',$s); // $mode=='back' for instance
      }
      return protection($r);
  }

/********
 * protects some names which are not allowable as an rclass for Wims
 * @param $s input string
 * @return the protected string
 */
 function protection($s){
   for ($i=0; $i<strlen($s); $i++){
     if (strstr("- ',;.:!=+",$s[$i])) $s[$i]="_";
   }
   return $s;
 }
	
?>
