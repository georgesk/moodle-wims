<?php  /// Moodle-Wims Configuration File 

$CFG->wimshost="localhost"; // the IP address for the Wims server
$CFG->wimsroot="wims";      // URL = "http://".$CFG->wimshost."/".$CFG->wimsroot
$CFG->wimsRemoteUrl="localhost/"; // the external URL for Wims
$CFG->protocol="localwims"; // the name of the configuration file for Wims
// this file is in ${WIMS_HOME}/log/classes/.connections/ , with the following password.
$CFG->passwims="azerty";    // a shared secret

?>
