<?php
$string['typewims'] = 'Wims';
$string['type_exe'] = 'Ordinary worksheet';
$string['type_exe_help'] = 'Ordinary worksheet or Exam session';
$string['add_this_sheet'] = 'Import this sheet';
$string['adminclass'] = 'Administate this Wims miniclass';
$string['adminsheet'] = 'Manage contents in this Wims miniclass';
$string['already_imported'] = 'X';
$string['apply'] = 'Go to the Wims miniclass';
$string['automatic_score'] = 'Automatic comment from Wims';
$string['closeandupdate'] = 'Close this window and update in Moodle';
$string['consider_imports'] = 'There are currently no worksheets in this miniclass.<br>Please consider importing some previously submitted worksheets.<br><b>See below</b>';
$string['create_an_exam'] = 'There are still no exam session defined. Please manage the contents of this miniclass.';
$string['exam'] = 'Exam session';
$string['exam_meanscore'] = 'Overall score for the exam:';
$string['examFeedback'] = 'List of exam sessions';
$string['getcsv'] = 'CSV file';
$string['getexamsource'] = 'Source';
$string['getsheetsource'] = 'Source';
$string['hello_designer'] = 'Exercise type';
$string['import_selected_sheets'] = 'Import the selected sheets';
$string['importableSheets'] = 'List of previously submitted worksheets (for imports)';
$string['listofsheets'] = 'Worksheets available in this Wims miniclass';
$string['makeasheet'] = 'There is still no worksheet. Please use the button above to create at least one sheet.';
$string['maybewimsbusy'] = 'Sometimes, Wims is busy during a while, to do some daily housekeeping. You may try creating the Wims miniclass a little later.';
$string['meansheet'] = 'Average score:';
$string['no_exam_yet'] = 'The teacher has not yet defined an exam. Please check later.';
$string['nosheetsyet'] = 'The teacher has not yet defined a worksheet. Please check later.';
$string['retry'] = 'Try again';
$string['score_is'] = 'score:&nbsp;';
$string['sessions_done'] = 'sessions have been finished.';
$string['sorrynoauthuser'] = 'Sorry, the authentification failed.';
$string['sorrynothing'] = 'Sorry, no contents were found for this assignment.';
$string['sorrynotopen'] = 'Sorry, you cannot enter because this assignment is not open.';
$string['user_data'] = 'User\'s data';

$string['defaultexercise'] = 'Ordinary worksheet';


?>

