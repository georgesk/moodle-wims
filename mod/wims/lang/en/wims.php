<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for wims
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_wims
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'wims';
$string['modulenameplural'] = 'wimss';
$string['modulename_help'] = 'Use the wims module for... | The wims module allows...';
$string['wims:addinstance'] = 'Add a new wims';
$string['wims:submit'] = 'Submit wims';
$string['wims:view'] = 'View wims';
$string['wimsfieldset'] = 'Custom example fieldset';
$string['wimsname'] = 'wims name';
$string['wimsname_help'] = 'This is the content of the help tooltip associated with the wimsname field. Markdown syntax is supported.';
$string['wims'] = 'wims';
$string['pluginadministration'] = 'wims administration';
$string['pluginname'] = 'wims';
