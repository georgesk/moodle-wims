# MOODLE-WIMS #

## WIMS ... ##

WIMS is a web service which provides interactive exercises for students
from the primary school to the university. It provides also an exam mode.

## MOODLE ... ##

MOODLE is a web service deploied in many high schools and universities,
to manage teaching activities.

## THIS PACKAGE ##

The package wims-moodle provides an easy way to configure both WIMS
and MOODLE engines existing on a same web server, so they can interact.

